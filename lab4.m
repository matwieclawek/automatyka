load('obiekt.mat');
%plot(y)
k=2.2;
T=17;
theta=7.5;
modelA=tf(k,[T,1],'OutputDelay', theta);
t=0:59;
ymodelA=step(modelA,t);
plot(t,y,t,ymodelA);

clear;
load('obiekt.mat');
figure;
K  = 2;
T1 = 14.8;
T2 = 1.04;
theta = 7.5; 

[parametry, blad] = fminsearch('ident',[K,T1, T2]);


licz = [0 0 K];
mian = [(T1 * T2) (T1 + T2)  1];

obj = tf(licz,mian);
set(obj,'outputdelay', theta)

%hold on;
t=0:59;
ymodel=step(obj,t);
plot(t,y,t,ymodel);
ident