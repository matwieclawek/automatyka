clear all;
[licznik, mianownik]=zp2tf([],[-1],1);
nyquist(licznik, mianownik);
figure;

[licznik, mianownik]=zp2tf([],[-1,0],1);
nyquist(licznik, mianownik);

figure;

[licznik, mianownik]=zp2tf([],[-1,0,0],1);
nyquist(licznik, mianownik);
